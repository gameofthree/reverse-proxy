#!/bin/sh

PORT="80"
if [ -n "$1" ]; then
  PORT="$1"
fi

SERVER_HOST="gameofthree-server"
if [ -n "$2" ]; then
  SERVER_HOST="$2"
fi

SERVER_PORT="80"
if [ -n "$3" ]; then
  SERVER_PORT="$3"
fi

CLIENT_HOST="gameofthree-client"
if [ -n "$4" ]; then
  CLIENT_HOST="$4"
fi

CLIENT_PORT="80"
if [ -n "$5" ]; then
  CLIENT_PORT="$5"
fi

IMAGE_NAME="gameofthree-reverse-proxy"
if [ -n "$6" ]; then
  IMAGE_NAME="$6"
fi

SCHEDULING_POOL_SIZE="8"
if [ -n "$7" ]; then
  SCHEDULING_POOL_SIZE="$7"
fi

DEVENV="--prod"
if [ -n "$8" ]; then
  DEVENV="$8"
fi


cd ../server/ || exit
./run.sh "$SERVER_PORT" "$SERVER_HOST" "$SCHEDULING_POOL_SIZE" &

cd ../client/ || exit
if [ "$DEVENV" = "--prod" ]; then
    ./run.sh "$CLIENT_PORT" localhost "$CLIENT_HOST" &
else
    ./run-dev.sh "$CLIENT_PORT" "$CLIENT_HOST" &
fi

while [ "$( docker container inspect -f '{{.State.Status}}' "$SERVER_HOST" )" != "running" ];
  do
    echo "[1] Waiting for container $SERVER_HOST to start..."
    sleep 1
  done

while [ "$( docker container inspect -f '{{.State.Status}}' "$CLIENT_HOST" )" != "running" ];
  do
    echo "[2] Waiting for container $CLIENT_HOST to start..."
    sleep 1
  done

cd ../reverse-proxy/ || exit
./run.sh "$PORT" "$SERVER_HOST" "$SERVER_PORT" "$CLIENT_HOST" "$CLIENT_PORT" "$IMAGE_NAME"
