#!/bin/sh

PORT="80"
if [ -n "$1" ]; then
  PORT="$1"
fi

SERVER_HOST="gameofthree-server"
if [ -n "$2" ]; then
  SERVER_HOST="$2"
fi

SERVER_PORT="80"
if [ -n "$3" ]; then
  SERVER_PORT="$3"
fi

CLIENT_HOST="gameofthree-client"
if [ -n "$4" ]; then
  CLIENT_HOST="$4"
fi

CLIENT_PORT="80"
if [ -n "$5" ]; then
  CLIENT_PORT="$5"
fi

IMAGE_NAME="gameofthree-reverse-proxy"
if [ -n "$6" ]; then
  IMAGE_NAME="$6"
fi

CONTAINER_NAME="$IMAGE_NAME"


docker container rm -f "$CONTAINER_NAME"
docker image rm -f "$IMAGE_NAME"

docker network create --driver bridge reverse_proxy_network || true
docker build -t "$IMAGE_NAME" .
docker run --network reverse_proxy_network --expose "$PORT" -p "$PORT":"$PORT" -e PORT="$PORT" -e SERVER_PORT="$SERVER_PORT" -e SERVER_HOST="$SERVER_HOST" -e CLIENT_HOST="$CLIENT_HOST" -e CLIENT_PORT="$CLIENT_PORT" --name "$CONTAINER_NAME" "$IMAGE_NAME"
