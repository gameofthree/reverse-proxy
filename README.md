# Reverse Proxy

This repository holds the main web server which acts as a reverse proxy. Its purpose is to serve the client application
and upgrade the connections to the server application. In other words, in order to have the whole client-server application
working, you need to use this repo to start the applications.

# run.sh
The `run.sh` will try to start the web server inside a *Docker* container. As a prerequisite, the other applications 
(client and server) need to be started as docker containers too. This can be done manually by invoking `server/run.sh` and
`client/run.sh`/`client/run-dev.sh` or by composing them up via `compose.sh` or `docker-compose.yml`

# compose.sh
The `compose.sh` has one single prerequisite: This project to be located in a folder named `reverse-proxy` and 
the server and the client app to be siblings of this folder, named respectively `server` and `client`.

```
 /
|
|
|-----home
|     |
|     |
|     |-----gameofthree
|           |   
|           |-----client
|           |   run.sh
|           |   compose.sh
|           |   
|           |-----reverse-proxy
|           |   run.sh
|           |   
|           |-----server
|           |   run.sh
|           |   
|       
```


Then the only thing needed is `./compose.sh`. Although it starts without parameters, it actually accepts parameters:

`./compose.sh {mainPort} {serverAppName} {serverAppPort} {clientAppName} {clientAppPort} {thisAppName} {serverThreadPoolSize} {devEnvironment}`

e.g.

`./compose.sh 80 gameofthree-server 80 gameofthree-client 80 gameofthree-reverse-proxy 8 --prod`


# Docker Compose
Same as `compose.sh`, docker-compose.yml config favors the run of all three containers via `docker-compose up`.
The folder structure requirement is still the same - `client`, `reverse-proxy` and `server` to be sibling directories.

